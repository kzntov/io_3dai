
if "bpy" in locals():
    import importlib
    importlib.reload(export_3ao)
else:
    from . import export_3ao

import bpy
from bpy_extras.io_utils import ExportHelper



bl_info = {
    "name":"import/export for 3d-ai",
    "author":"kzn@3d-ai.com",
    "version":(0,3),
    "blender":(2,79,0),
    "location":"File > Import-Export",
    "description":"exporter / importer",
    "warning":"",
    "wiki_url":"",
    "support":"TESTING",
    "category": "Import-Export"
}


class Export3ao(bpy.types.Operator, ExportHelper):
    bl_idname = "export.3ao"
    bl_label = "export 3ao"
    bl_options = {"REGISTER","UNDO"}
    filename_ext = ".3ao"

    prop_export_bone = bpy.props.BoolProperty(
        name = "export bones",
        default = True
    )
    prop_export_morph = bpy.props.BoolProperty(
        name = "export shape_keys",
        default = True
    )

    prop_export_unhide_mesh = bpy.props.BoolProperty(
        name = "export only displayed mesh",
        default = True
    )

    prop_export_unhide_bone = bpy.props.BoolProperty(
        name = "export only displayed armature(bone)",
        default = True
    )

    def execute(self, context):

        #switch object mode
        if bpy.context.mode != "OBJECT":
            bpy.ops.object.mode_set(mode="OBJECT")

        meshes,vid_tables = export_3ao.makeMeshes(bpy.data.objects, self.prop_export_unhide_mesh)

        bone_id = {}
        bones = {}
        if self.prop_export_bone:
            bone_id, bones = export_3ao.getBones(self.prop_export_unhide_bone)

        shape_keys = {}
        if self.prop_export_morph:
            shape_keys = export_3ao.getShapeKeys(meshes,bpy.data.objects,vid_tables, self.prop_export_unhide_mesh)


        ai3d_obj = {
            "mesh":meshes,
            "bone":bones,
            "bone_id":bone_id,
            "morph":shape_keys,
            "rigid_body":{}
        }

        ai3d_obj = export_3ao.blenderAxisToGLAxis(ai3d_obj)

        #check validation of bone_list and weights
        ai3d_obj = export_3ao.checkValidation(self, ai3d_obj)



        export_3ao.write(self.filepath,ai3d_obj)

        return {'FINISHED'}


def menu_func_export(self, context):
    self.layout.operator(Export3ao.bl_idname, text="3d-ai (.3ao)")


def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_export.append(menu_func_export)


def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_export.remove(menu_func_export)

if __name__ == "__main__":
    register()
