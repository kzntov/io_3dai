import json
import bpy
from copy import deepcopy
from collections import OrderedDict


def write(filepath, model):
    try:
        fp = open(filepath, 'w')
        json.dump(model,fp)
        print("save")
    finally:
        fp.close()


def makeMeshes(bl_objects, prop_export_unhide):
    meshes = []
    vid_tables = {}
    for bl_object in bl_objects:

        if bl_object.type != "MESH":
            continue
        if bl_object.hide and prop_export_unhide:
            continue

        print("get mesh from ["+bl_object.name+"]")
        bl_mesh = bl_object.data

        #get bone_weights and bone_indices
        bone_indices, bone_weights = getBoneWeights(bl_object)

        #get vertices and normals
        comp_vertices = []#vertices[n] = {v:vec3,n:vec3,u:vec2}
        for v_id,vertex in enumerate(bl_mesh.vertices):
            comp_vertices.append({
                "v":vertex.co,
                "n":vertex.normal,
                "b_w":bone_weights[v_id],
                "b_i": bone_indices[v_id]
            })


        #uv and index
        indices = []
        uvid_list = {}#uv_id_list[u+v] = index
        vid_table = {}#new_vid_list[old_v_id][uv_id] = new_vertices_index
        uv_name = None
        if len(bl_mesh.uv_textures) > 0:
            uv_name = bl_mesh.uv_textures[0].name
        else:#uv doest not exists,initialize 0
            uvid_list["0/0"] = 0

        for polygon in bl_mesh.polygons:
            uv_index = polygon.loop_start
            pol_count = polygon.loop_total
            if pol_count < 3:
                print("error")
                continue

            for i in range(pol_count-2):
                loop = [0,1+i,2+i]
                for index in loop:
                    if uv_name is None:
                        v_id = polygon.vertices[index]
                        new_v_id, vid_table, comp_vertices = VidUvid2NewVid(v_id, 0, vid_table, comp_vertices)
                        indices.append(new_v_id)
                    else:
                        uv = bl_mesh.uv_layers[uv_name].data[uv_index+index].uv
                        uv_id,uvid_list = uv2uvid(uv,uvid_list)
                        v_id = polygon.vertices[index]
                        new_v_id,vid_table,comp_vertices = VidUvid2NewVid(v_id, uv_id,vid_table,comp_vertices)
                        indices.append(new_v_id)
        vid_tables[bl_mesh.name] = vid_table

        #serialize
        vertices = []
        normals = []
        uvs = []
        bone_list = []
        bone_weights = []
        uvid2value = {v: k for k, v in uvid_list.items()}
        for vertex in comp_vertices:
            vertices.append(vertex["v"][0])
            vertices.append(vertex["v"][1])
            vertices.append(vertex["v"][2])
            normals.append(vertex["n"][0])
            normals.append(vertex["n"][1])
            normals.append(vertex["n"][2])

            uv = [0.0,0.0]
            if "uv" in vertex:
                uv = uvid2value[vertex["uv"]].split("/")
            uvs.append(uv[0])
            uvs.append(1.0-float(uv[1]))
            bone_list.append(vertex["b_i"][0])
            bone_list.append(vertex["b_i"][1])
            bone_list.append(vertex["b_i"][2])
            bone_list.append(vertex["b_i"][3])
            bone_weights.append(vertex["b_w"][0])
            bone_weights.append(vertex["b_w"][1])
            bone_weights.append(vertex["b_w"][2])
            bone_weights.append(vertex["b_w"][3])

        mesh = {
            "name": bl_object.name,
            "vertices": vertices,
            "normals": normals,
            "indices": indices,
            "uvs": uvs,
            "bone_list":bone_list,
            "bone_weight":bone_weights,
            "material":{
                "texture":"",
                "ambient":[0.5, 0.5, 0.5],
                "diffuse":[1,1,1],
                "specular":{
                    "color":[1,1,1],
                    "factor":50
                }
            }
        }

        #material exists
        if len(bl_mesh.materials) > 0:
            material = bl_mesh.materials[0]
            if hasattr(material.active_texture,"image") and hasattr(material.active_texture.image,"name"):
                mesh["material"]["texture"] = material.active_texture.image.name
            mesh["material"]["ambient"] = [material.ambient, material.ambient, material.ambient]
            diffuse_intensity = material.diffuse_intensity
            diffuse = material.diffuse_color
            mesh["material"]["diffuse"] = [diffuse[0]*diffuse_intensity, diffuse[1]*diffuse_intensity, diffuse[2]*diffuse_intensity]
            specular_intensity = material.specular_intensity
            specular = material.specular_color
            mesh["material"]["specular"] = {
                "color":[specular[0]*specular_intensity, specular[1]*specular_intensity, specular[2]*specular_intensity],
                "factor":material.specular_hardness
            }
            mesh["material"]["shadeless"] = material.use_shadeless

        #trs convert
        if hasattr(bl_object,"scale"):
            scale = bl_object.scale
            mesh = applyTRS(mesh, scale)

        meshes.append(mesh)
    return meshes,vid_tables


def applyTRS(mesh, scale):
    for i in range(int(len(mesh["vertices"])/3)):
        mesh["vertices"][i*3] = mesh["vertices"][i*3] * scale[0]
        mesh["vertices"][i * 3+1] = mesh["vertices"][i * 3+1] * scale[1]
        mesh["vertices"][i * 3+2] = mesh["vertices"][i * 3+2] * scale[2]
    return mesh


def uv2uvid(uv, uvid_list):
    key = str(round(uv[0],3)) +"/"+ str(round(uv[1],3))
    if key in uvid_list:
        return uvid_list[key], uvid_list
    else:
        uvid_list[key] = len(uvid_list)+1
        return uvid_list[key], uvid_list

def VidUvid2NewVid(v_id, uv_id,new_vid_list,vertices):
    if v_id in new_vid_list and uv_id in new_vid_list[v_id]:
        return new_vid_list[v_id][uv_id],new_vid_list,vertices
    else:#verticesの追加
        if v_id in new_vid_list:#add
            new_vertex = deepcopy(vertices[v_id])
            new_vertex["uv"] = uv_id
            vertices.append(new_vertex)
            new_vid_list[v_id][uv_id] = len(vertices)-1
        else:#書き換え
            vertices[v_id]["uv"] = uv_id
            new_vid_list[v_id] = {uv_id:v_id}
        return new_vid_list[v_id][uv_id],new_vid_list,vertices

def getBones(unhide_displayed):
    bones = {}
    bone_id = []
    ik_bones = {}

    for object in bpy.data.objects:
        if object.type != "ARMATURE":
            continue
        if object.hide and unhide_displayed:
            continue

        for bone in object.data.bones:
            bone_id.append(bone.name)

            lock_rotation = object.pose.bones[bone.name].lock_rotation
            lock_location = object.pose.bones[bone.name].lock_location

            bones[bone.name] = {
                "bone_mat":[1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1],
                "offset":[],
                "add_rot":0,
                "add_move":0,
                "linkage_bone":"",
                "linkage_bone_coef":0,
                "ik":0,  #temporarily invalidate on first loop
                "ik_target_name":"",
                "physics":0,
                "connection":"position",
                "connect_position":[
                    bone.tail_local[0]-bone.head_local[0],
                    bone.tail_local[2]-bone.head_local[2],
                    -(bone.tail_local[1]-bone.head_local[1])
                ],
                "lock_rotation":[int(lock_rotation[0]),int(lock_rotation[1]),int(lock_rotation[2])],
                "lock_translation":[int(lock_location[0]), int(lock_location[1]), int(lock_location[2])],
                "visible":1,
                "pos":[bone.head_local[0],bone.head_local[2],-bone.head_local[1]]
            }

            if bone.parent is not None:
                bones[bone.name]["parent"] = bone.parent.name
                bones[bone.name]["bone_mat"] = [
                    1,0,0,bone.head_local[0]-bone.parent.head_local[0],
                    0, 1, 0, bone.head_local[2]-bone.parent.head_local[2],
                    0, 0, 1, -(bone.head_local[1]-bone.parent.head_local[1]),
                    0, 0, 0, 1,
                ]

            bones[bone.name]["offset"] = [
                1,0,0,-bone.head_local[0],
                0, 1, 0, -bone.head_local[2],
                0, 0, 1, bone.head_local[1],
                0, 0, 0, 1,
            ]
            #if ik setting exists, add to `ik_bones`
            if "IK" in object.pose.bones[bone.name].constraints:
                ik_setting = object.pose.bones[bone.name].constraints["IK"]

                ik_bones[ik_setting.subtarget] = {"setting":ik_setting, "link":bone.name}

        #ik setting
        for ik_bone_name in ik_bones:
            bones[ik_bone_name]["ik"] = 1
            bones[ik_bone_name]["ik_target_name"] = ik_bone_name
            bones[ik_bone_name]["ik_link"] = []

            ik_setting = ik_bones[ik_bone_name]["setting"]
            link_bone_name = ik_bones[ik_bone_name]["link"]
            for i in range(ik_setting.chain_count):
                ik_bone = object.pose.bones[link_bone_name]
                rot_limit = 0
                top_limit = [0,0,0]
                bottom_limit = [0,0,0]
                if ik_bone.use_ik_limit_x is True:
                    rot_limit = 1
                    top_limit[0] = ik_bone.ik_max_x
                    bottom_limit[0] = ik_bone.ik_min_x
                if ik_bone.use_ik_limit_y is True:
                    rot_limit = 1
                    top_limit[1] = ik_bone.ik_max_y
                    bottom_limit[1] = ik_bone.ik_min_y
                if ik_bone.use_ik_limit_z is True:
                    rot_limit = 1
                    top_limit[2] = ik_bone.ik_max_z
                    bottom_limit[2] = ik_bone.ik_min_z


                bones[ik_bone_name]["ik_link"].append({
                    "link_name":link_bone_name,
                    "limit":rot_limit,
                    "top_limit":top_limit,
                    "bottom_limit":bottom_limit
                })
                link_bone_name = bones[link_bone_name]["parent"]




    return bone_id,bones


def getBoneWeights(object):
    bone_weights = []
    bone_indices = []
    v_groups = object.vertex_groups#v_groups[n] = bone_name
    for vertex in object.data.vertices:
        bone_index = []
        bone_weight = []
        #get weight and index
        for i,bone_group in enumerate(vertex.groups):
            #add to array except too small weight
            if bone_group.weight > 0.01:
                bone_weight.append(bone_group.weight)
                bone_index.append(bone_group.group)
            if len(bone_index) == 4:
                break

        #normalize
        weight_sum = 0.0
        for weight in bone_weight:
            weight_sum += weight
        #if weight_sum is greater than 0, normalize 1
        if weight_sum > 0.1:
            for i,weight in enumerate(bone_weight):
                bone_weight[i] *= (1/weight_sum)
                if bone_weight[i] <0.001:
                    bone_weight[i] = 0.0
                elif bone_weight[i] >0.999:
                    bone_weight[i] = 1.0
            # padding
            for index in range(4-len(bone_weight)):
                bone_index.append(-1)
                bone_weight.append(0)
        else:
            bone_weight = [1,0,0,0]
            bone_index = [-1,-1,-1,-1]
            print("warning: weight_sum is too small")
        bone_weights.append(bone_weight)
        bone_indices.append(bone_index)

    return bone_indices, bone_weights


def getShapeKeys(meshes, bl_objects,vid_tables, prop_export_unhide):
    name2id = {}
    shape_keys = {}
    for i,mesh in enumerate(meshes):
        name2id[mesh["name"]] = i

    for bl_object in bl_objects:
        if bl_object.name in name2id is None:
            continue
        if bl_object.hide and prop_export_unhide:
            continue

        mesh = bl_object.data
        if bl_object.type != "MESH":
            continue
        if mesh.shape_keys is None:
            continue

        #trs convert
        scale = [1,1,1]
        if hasattr(bl_object,"scale"):
            scale = bl_object.scale


        base_key_block = mesh.shape_keys.key_blocks[0]
        for i,key_block in enumerate(mesh.shape_keys.key_blocks):
            if i == 0:
                continue

            #initialize shape_keys[morph_name]
            if key_block.name not in shape_keys:
                shape_keys[key_block.name] = {
                    "type":"vertex",
                    "values":[]
                }

            for v_id,value in enumerate(key_block.data):
                change_amout = [
                    value.co[0]-base_key_block.data[v_id].co[0],
                    value.co[1] - base_key_block.data[v_id].co[1],
                    value.co[2] - base_key_block.data[v_id].co[2]
                ]

                #if change_amout is too small, don't append
                if abs(change_amout[0]) > 0.0001 or abs(change_amout[1]) > 0.0001 or abs(change_amout[2]) > 0.0001:
                    if v_id not in vid_tables[mesh.name]:
                        print("warning: uncontained vertex["+str(v_id)+"] in "+mesh.name)
                        continue
                    for uv_id in vid_tables[mesh.name][v_id]:#vid -> new vids
                        converted_vid = vid_tables[mesh.name][v_id][uv_id]
                        shape_value = [
                            name2id[bl_object.name],
                            converted_vid,
                            change_amout[0]*scale[0],
                            change_amout[1]*scale[1],
                            change_amout[2]*scale[2],
                        ]

                        shape_keys[key_block.name]["values"].append(shape_value)

    #sort key
    sorted_shape_keys = OrderedDict()
    sorted_keys = sorted(list(shape_keys.keys()))
    for key in sorted_keys:
        sorted_shape_keys[key] = shape_keys[key]


    return sorted_shape_keys

def getRigidBodies(bl_objects):
    rigid_bodies = {}

    for object in bl_objects:
        if object.rigid_body is None:
            continue

        rigid_body = {}
        if object.rigid_body.collision_shape == "SPHERE":
            rigid_body["shape"] = 0
        elif object.rigid_body.collision_shape == "BOX":
            rigid_body["shape"] = 1
        elif object.rigid_body.collision_shape == "CAPSULE":
            rigid_body["shape"] = 2
        else:
            print("error:unsupported shape `"+object.rigid_body.collision_shape+"` in `"+object.name+"`")

        

        rigid_bodies[object.name] = rigid_body
    return rigid_bodies

def blenderAxisToGLAxis(obj):
    for mesh_id,mesh in enumerate(obj["mesh"]):
        for i in range(int(len(mesh["vertices"])/3)):
            y_pos = mesh["vertices"][i*3+1]
            z_pos = mesh["vertices"][i*3+2]
            ny_pos = mesh["normals"][i*3+1]
            nz_pos = mesh["normals"][i*3+2]
            obj["mesh"][mesh_id]["vertices"][i*3+1] = z_pos
            obj["mesh"][mesh_id]["vertices"][i*3+2] = -y_pos
            obj["mesh"][mesh_id]["normals"][i * 3 + 1] = nz_pos
            obj["mesh"][mesh_id]["normals"][i * 3 + 2] = -ny_pos
    for morph_key in obj["morph"]:
        morph = obj["morph"][morph_key]
        for v_id,v_values in enumerate(morph["values"]):
            y_pos = v_values[3]
            z_pos = v_values[4]
            v_values[3] = z_pos
            v_values[4] = -y_pos
            obj["morph"][morph_key]["values"][v_id] = v_values
    return obj

def checkValidation(self, obj):
    error = False
    error_mesh_id = -1
    max_value = 0
    bone_max_id = len(obj["bone_id"])-1
    for mesh_id,mesh in enumerate(obj["mesh"]):
        # if bone_id in mesh is out of range of bone numbers, round within range
        for bone_list_index, bone_id in enumerate(mesh["bone_list"]):
            if bone_id > bone_max_id:
                obj["mesh"][mesh_id]["bone_list"][bone_list_index] = -1
                error = True

    if error:
        self.report({'ERROR'}, "Bone that do not exist is included in %s's Vertex Groups" %(obj["mesh"][error_mesh_id]["name"]))

    return obj